"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const pathResolver = require("path");
class OutputWriter {
    constructor(path, delim) {
        this.path = path;
        this.delim = delim;
        this.fileLines = [];
    }
    // tslint:disable-next-line:ban-types
    outputLineString(lines) {
        this.fileLines.push(lines.join(this.delim));
    }
    writeToFile() {
        fs.writeFileSync(pathResolver.resolve(__dirname, this.path), this.fileLines.join('\r\n'), { encoding: "utf-8" });
    }
}
exports.default = OutputWriter;
