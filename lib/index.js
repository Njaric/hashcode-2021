"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const input_reader_1 = require("./input-reader");
const output_writer_1 = require("./output-writer");
const fileName = 'd.txt';
const inputReader = new input_reader_1.default(`../input/${fileName}`, ' ');
const outputWriter = new output_writer_1.default(`../output/${fileName}`, ' ');
const [totalDuration, amountOfIntersections, amountOfStreets, amountOfCars, pointsPerCar] = inputReader.readLine().map(x => +x);
const intersections = [];
const streets = [];
const cars = [];
function getMinMaxOutliers(values) {
    values.sort(function (a, b) {
        return a - b;
    });
    var q1 = values[Math.floor((values.length / 4))];
    // Likewise for q3.
    var q3 = values[Math.ceil((values.length * (3 / 4)))];
    var iqr = q3 - q1;
    // Then find min and max values
    var max = q3 + iqr * 1.5;
    var min = q1 - iqr * 1.5;
    // Then return
    return { min, max };
}
for (let i = 0; i < amountOfIntersections; i++) {
    intersections.push({
        id: i,
        incoming: [],
        outgoing: [],
        streetOrder: [],
        currentStreetIndex: 0
    });
}
const amountOfCarsInStreet = {};
for (let i = 0; i < amountOfStreets; i++) {
    const [start, end, name, duration] = inputReader.readLine();
    const startIntersection = intersections.find(x => x.id === +start);
    const endIntersection = intersections.find(x => x.id === +end);
    const street = {
        start: startIntersection,
        end: endIntersection,
        name,
        duration: +duration,
        carsWaitingAtTheEnd: []
    };
    amountOfCarsInStreet[name] = 0;
    streets.push(street);
    startIntersection.outgoing.push(street);
    endIntersection.incoming.push(street);
}
// for (let i = 0; i < amountOfCars; i++) {
//     const [_, firstStr, ...str] = inputReader.readLine();
//     const car: Car = {
//         streets: str.map(x => streets.find(s => s.name === x)),
//         secondsRemainingOnStreet: 0
//     }
//     cars.push(car);
//     streets.find(x => x.name === firstStr).carsWaitingAtTheEnd.push(car);
// }
// let carsOnStreets: Car[] = []
for (let i = 0; i < amountOfCars; i++) {
    const [_, __, ...str] = inputReader.readLine();
    for (const s of str) {
        amountOfCarsInStreet[s]++;
    }
}
for (const intersection of intersections) {
    const valuableStreets = intersection.incoming.filter(x => amountOfCarsInStreet[x.name] > 0);
    const { min, max } = getMinMaxOutliers(valuableStreets.map(x => amountOfCarsInStreet[x.name]));
    debugger;
    for (const street of valuableStreets) {
        const value = amountOfCarsInStreet[street.name];
        intersection.streetOrder.push(street);
        if (value > max)
            intersection.streetOrder.push(street);
    }
}
// for (let sec = 0; sec < totalDuration; sec++) {
//
//     const carsToAddToStreet: Car[] = [];
//
//     for (const intersection of intersections) {
//         // Evaluate intersection
//
//         const street = intersection.streetOrder[intersection.currentStreetIndex];
//         if (++intersection.currentStreetIndex === intersection.streetOrder.length) {
//             intersection.currentStreetIndex = 0;
//         }
//         const movingCar = street.carsWaitingAtTheEnd[0];
//
//         if (movingCar) {
//             street.carsWaitingAtTheEnd.shift();
//             carsToAddToStreet.push(movingCar);
//             movingCar.streets.shift();
//             movingCar.secondsRemainingOnStreet = street.duration;
//         }
//     }
//
//     for (const car of carsOnStreets) {
//         car.secondsRemainingOnStreet--;
//         if (car.secondsRemainingOnStreet === 0) {
//
//             carsOnStreets = carsOnStreets.filter(x => x !== car);
//
//             // If car is donzo, get the heck outta here
//             if (car.streets.length === 0) {
//                 continue;
//             }
//
//             car.streets[0].carsWaitingAtTheEnd.push(car);
//             car.streets.shift();
//         }
//     }
//
//     carsOnStreets = [...carsOnStreets, ...carsToAddToStreet];
//
// }
outputWriter.outputLineString([intersections.length]);
for (const intersection of intersections) {
    outputWriter.outputLineString([intersection.id]);
    const kappa = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < intersection.streetOrder.length; i++) {
        const street = intersection.streetOrder[i];
        let val = kappa.find(x => x.streetName === street.name);
        if (!val) {
            val = {
                streetName: street.name,
                seconds: 0
            };
            kappa.push(val);
        }
        val.seconds++;
    }
    outputWriter.outputLineString([kappa.length]);
    for (const kappaElement of kappa) {
        outputWriter.outputLineString([kappaElement.streetName, kappaElement.seconds]);
    }
}
outputWriter.writeToFile();
