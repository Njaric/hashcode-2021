"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const pathResolver = require("path");
class InputReader {
    constructor(path, delim) {
        this.path = path;
        this.delim = delim;
        const fileString = fs.readFileSync(pathResolver.resolve(__dirname, this.path), { encoding: "utf-8" });
        this.fileLines = fileString.split('\n');
        this.nextLinesToRead = 0;
    }
    readLine() {
        return this.fileLines[this.nextLinesToRead++].split(this.delim);
    }
    hasNextLine() {
        return this.fileLines.length > this.nextLinesToRead;
    }
}
exports.default = InputReader;
