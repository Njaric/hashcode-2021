import * as fs from 'fs';
import * as pathResolver from 'path'

export default class InputReader {
    private fileLines: string[];
    private nextLinesToRead: number;


    constructor(private path: string, private delim: string) {
        const fileString = fs.readFileSync(pathResolver.resolve(__dirname, this.path), {encoding: "utf-8"});

        this.fileLines = fileString.split('\n');
        this.nextLinesToRead = 0;
    }

    readLine() {

        return this.fileLines[this.nextLinesToRead++].split(this.delim);
    }

    hasNextLine() {
        return this.fileLines.length > this.nextLinesToRead;
    }
}


