import * as fs from 'fs';
import * as pathResolver from 'path'

export default class OutputWriter {
    private fileLines: string[];
    private nextLinesToRead: number;


    constructor(private path: string, private delim: string) {
        this.fileLines = [];
    }

    // tslint:disable-next-line:ban-types
    outputLineString(lines: Object[]){
        this.fileLines.push(lines.join(this.delim))
    }

    writeToFile(){
        fs.writeFileSync(pathResolver.resolve(__dirname,this.path),this.fileLines.join('\r\n'),{encoding: "utf-8"})
    }
}


